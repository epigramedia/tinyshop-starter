import sys
from fabric.api import *

env.hosts = ['root@<YOUR_SERVER_ADDRESS_OR_IP>:<YOUR_SSH_PORT>']

USER = '{{ project_name }}'
GROUP = 'www-data' # usually: www-data

APP_NAME = '{{ project_name }}' # usually equals to USER
APP_DIR = '/home/%s/public_html/%s' % (USER, APP_NAME) # FIT FOR YOURS
DB_NAME = '%s.db' % APP_NAME # or False
MEDIA_DIR = '%s/media' % APP_DIR
STATIC_DIR = '%s/static' % APP_DIR

CLONE_COMMAND = 'git clone <YOUR_REPOSITORY_ADDRESS>'

def restart_server(nginx=False):
    run('supervisorctl reread')
    run('supervisorctl update')
    run('supervisorctl restart %s' % APP_NAME)
    if nginx:
        run('service nginx restart %s' % APP_NAME)

def refresh_static(update_package=False):
    with cd(APP_DIR):
        run('git checkout .')
        run('git pull origin master')

        if update_package:
            run('env/bin/pip install -r requirements.txt --upgrade')
            run('env/bin/python manage.py syncdb')

        run('env/bin/python manage.py collectstatic --noinput')

        with warn_only():
            for x in [MEDIA_DIR, STATIC_DIR]:
                run('chown -R %s:%s %s' % (USER, GROUP, x))
                run('find %s -type d -exec chmod 775 {} \;' % x)
                run('find %s -type f -exec chmod 664 {} \;' % x)

def refresh():
    refresh_static(update_package=True)
    restart_server()

def setup_server():
    # check public_html, crate if not exist
    with cd('/home/%s' % USER):
        with warn_only():
            run('mkdir public_html')
        run('chown -R %s:%s public_html' % (USER, GROUP))
        run('chmod 775 public_html')

    # clone the repository if not exist
    with warn_only():
        result = run('[ -d %s ]' % APP_DIR)
        if result.return_code > 0:
            with cd('/home/%s/public_html' % USER):
                run(CLONE_COMMAND)

    # change mode and ownership of app_dir
    run('chmod 775 %s' % APP_DIR)
    run('chown %s:%s %s' % (USER, GROUP, APP_DIR))

    with cd(APP_DIR):
        # pull
        run('git checkout .')
        run('git pull origin master')

        # copy nginx and supervisor settings
        run('cp -f confserver/nginx.conf /etc/nginx/sites-available/%s.conf' % APP_NAME)
        run('ln -s -f /etc/nginx/sites-available/%s.conf /etc/nginx/sites-enabled/' % APP_NAME)
        run('cp -f confserver/supervisor.conf /etc/supervisor/conf.d/%s.conf' % APP_NAME)

        # install virtualenv and pip
        with warn_only():
            result = run('[ -d env ]')
            if result.return_code > 0:
                run('virtualenv env')
                run('env/bin/pip install pip==1.5.6')
                # run('env/bin/pip install pip==1.3.1') # i still love old PIP

        # fix the easy setup
        # run('env/bin/python ez_setup.py')

        # install requirements
        run('env/bin/pip install -r requirements.txt --upgrade')

        # syncdb and collectstatic
        run('env/bin/python manage.py syncdb')
        run('env/bin/python manage.py collectstatic --noinput')

        with warn_only():
            for x in [MEDIA_DIR, STATIC_DIR]:
                run('chown -R %s:%s %s' % (USER, GROUP, x))
                run('find %s -type d -exec chmod 775 {} \;' % x)
                run('find %s -type f -exec chmod 664 {} \;' % x)

        # change mode and ownership of db_name if exists
        if DB_NAME:
            with warn_only():
                run('chown %s:%s %s' % (USER, GROUP, DB_NAME))
                run('chmod 664 %s' % DB_NAME)

    restart_server(nginx=True)