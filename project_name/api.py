from tinyshop.api import ShopAPI as TinyShopAPI

class ShopAPI(TinyShopAPI):
    def validate_for_cart(self, request, product, sku, quantity=1, for_update=False):
        cart_item = self.get_cart_item(request, product, sku)
        if for_update:
            # if for update, then
            # check if quantity WILL equal to product's minimum order

            if cart_item is None:
                return False

            return (quantity >= product.minimum_order) or (quantity == 0)

        # if not for update, then
        # check quantity if larger or equals to product's minimum order
        existing_quantity = 0
        if cart_item is not None:
            existing_quantity = cart_item.quantity

        quantity_to_be = existing_quantity + quantity

        return (existing_quantity + quantity >= product.minimum_order) or (quantity_to_be == 0)

shop_api = ShopAPI()