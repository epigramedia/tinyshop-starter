# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import taggit.managers
import mptt.fields
import jsonfield.fields
import tinyshop.models
import django.contrib.auth.models
import django.db.models.deletion
import django.utils.timezone
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0001_initial'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, max_length=30, verbose_name='username', validators=[django.core.validators.RegexValidator(b'^[\\w.@+-]+$', 'Enter a valid username.', b'invalid')])),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('email', models.EmailField(unique=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('extra_data', jsonfield.fields.JSONField(null=True, blank=True)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
            managers=[
                (b'objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('slug', models.SlugField(unique=True, verbose_name='Slug', blank=True)),
                ('picture', models.ImageField(upload_to=b'', null=True, verbose_name='Picture', blank=True)),
                ('is_published', models.BooleanField(default=True, verbose_name='Is Published')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('extra_data', jsonfield.fields.JSONField(null=True, blank=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Parent', blank=True, to='{{ project_name }}.Category', null=True)),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title', blank=True)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug', blank=True)),
                ('picture', models.ImageField(upload_to=b'', null=True, verbose_name='Picture', blank=True)),
                ('is_published', models.BooleanField(default=True, verbose_name='Is Published')),
                ('position', models.IntegerField(default=0, verbose_name='Positions', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('extra_data', jsonfield.fields.JSONField(null=True, blank=True)),
                ('excerpt', models.TextField(null=True, verbose_name='Excerpt', blank=True)),
                ('content', models.TextField(null=True, verbose_name='Content', blank=True)),
                ('start_publish', models.DateTimeField(null=True, verbose_name='Start Publish', blank=True)),
                ('end_publish', models.DateTimeField(null=True, verbose_name='End Publish', blank=True)),
                ('seo_keyword', models.CharField(max_length=255, null=True, verbose_name='SEO Keyword', blank=True)),
                ('seo_description', models.CharField(max_length=255, null=True, verbose_name='SEO Description', blank=True)),
                ('unit_price', models.DecimalField(max_digits=14, decimal_places=0)),
                ('pre_order', models.BooleanField(default=True)),
                ('minimum_order', models.PositiveIntegerField(default=1)),
                ('sku', models.CharField(unique=True, max_length=100, blank=True)),
                ('author', models.ForeignKey(verbose_name='Author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('categories', models.ManyToManyField(to='{{ project_name }}.Category', blank=True)),
                ('related_products', models.ManyToManyField(related_name='related_products_rel_+', to='{{ project_name }}.Product', blank=True)),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags')),
            ],
            options={
                'ordering': ['-created'],
                'abstract': False,
            },
            bases=(tinyshop.models.ProductInterface, models.Model),
        ),
    ]
