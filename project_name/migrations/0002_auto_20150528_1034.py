# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tinyshop', '0002_auto_20150526_1459'),
        ('{{ project_name }}', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='address',
            field=models.OneToOneField(null=True, blank=True, to='tinyshop.Address'),
        ),
        migrations.AlterField(
            model_name='product',
            name='position',
            field=models.IntegerField(default=0, verbose_name='Positions', blank=True),
        ),
    ]
