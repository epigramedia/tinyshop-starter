from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from tinycms.base_models import (
    BaseUser,
    BaseCategory,
    BasePost,
    # BaseImage
)

from tinyshop.models import Address, ProductInterface


class User(BaseUser):
    address = models.OneToOneField('tinyshop.Address', null=True, blank=True)
    class Meta:
        abstract = False


class Category(BaseCategory):
    class Meta(BaseCategory.Meta):
        abstract = False


# class Image(BaseImage):
#     class Meta(BaseImage.Meta):
#         abstract = False


class Product(ProductInterface, BasePost):
    categories = models.ManyToManyField(settings.CMS_MODEL_CATEGORY, blank=True)
    unit_price = models.DecimalField(max_digits=14, decimal_places=0)
    related_products = models.ManyToManyField('self', blank=True)
    pre_order = models.BooleanField(default=True)

    minimum_order = models.PositiveIntegerField(default=1)

    sku = models.CharField(max_length=100, blank=True, unique=True)
    
    class Meta(BasePost.Meta):
        abstract = False

    def __unicode__(self):
        return self.title

    def get_main_image(self):
        main_image = self.images.all()

        if main_image:
            main_image = main_image[0]

            return main_image.image
        else:
            return None

    def get_absolute_url(self):
        return reverse('product_detail', args=(self.slug,))

    def get_product_name(self, sku=None):
        return self.title

    def get_option_list(self):
        return None

    def get_option_text(self, sku=None):
        return ''

    def get_unit_price(self, sku=None, user=None, quantity=1):
        return self.unit_price

    def validate_sku(self, sku):
        return self.sku == sku