from tinycms.resources import (
    BaseUserResource,
    BaseCategoryResource,
    # BaseImageResource,
    BasePostResource
)

from .models import *
from tinyshop.models import (
    Cart, CartItem,
    ExtraPriceCart, ExtraPriceCartItem,
    BankAccount, PaymentConfirmation,
    Address
)

from tinyshop.resources import ShopResource as TinyShopResource
from tastypie import validator
from tastypie.resources import fields, ModelResource
from tastypie import authorization


class UserResource(BaseUserResource):
    class Meta(BaseUserResource.Meta):
        pass


class CategoryResource(BaseCategoryResource):
    class Meta(BaseCategoryResource.Meta):
        pass


# class ImageResource(BaseImageResource):
#     class Meta(BaseImageResource.Meta):
#         pass


class ProductResource(BasePostResource):
    class Meta:
        queryset = Product._default_manager.all()
        resource_name = 'product'


class CartResource(ModelResource):
    billing_address = fields.ToOneField('{{ project_name }}.resources.AddressResource', 'billing_address', null=True)
    shipping_address = fields.ToOneField('{{ project_name }}.resources.AddressResource', 'shipping_address', null=True)
    cart_items = fields.ToManyField('{{ project_name }}.resources.CartItemResource', 'cartitem_set', null=True, related_name='cart')
    extra_price = fields.ToManyField('{{ project_name }}.resources.ExtraPriceCartResource', 'extrapricecart_set', related_name='cart', null=True)
    
    get_total = fields.DecimalField(attribute='get_total', null=True, readonly=True)
    get_subtotal = fields.DecimalField(attribute='get_subtotal', null=True, readonly=True)
    get_extra_price = fields.DecimalField(attribute='get_extra_price', null=True, readonly=True)
    get_total_payment = fields.DecimalField(attribute='get_total_payment', null=True, readonly=True)
    get_remaining_payment = fields.DecimalField(attribute='get_remaining_payment', null=True, readonly=True)
    class Meta:
        queryset = Cart.objects.all()
        resource_name = 'cart'
        authorization = authorization.StaffAuthorization(all=True)
        filtering = {
            'order_status': ['in'],
            'is_order': ['exact'],
            'order_reference': ['exact']
        }
        ordering = ['order_reference', 'order_status', 'payment_status']
        fuzzy_fields = ['order_reference', 'billing_address__name']


class CartItemResource(ModelResource):
    cart = fields.ForeignKey('{{ project_name }}.resources.CartResource', 'cart')
    product = fields.ForeignKey('{{ project_name }}.resources.ProductResource', 'product')
    extra_price = fields.ToManyField('{{ project_name }}.resources.ExtraPriceCartItemResource', 'extrapricecartitem_set', related_name='cart', null=True)
    get_subtotal = fields.DecimalField(attribute='get_subtotal', null=True, readonly=True)
    get_total = fields.DecimalField(attribute='get_total', null=True, readonly=True)
    class Meta:
        queryset = CartItem.objects.all()
        resource_name = 'cartitem'
        authorization = authorization.StaffAuthorization(all=True)


class ExtraPriceCartResource(ModelResource):
    cart = fields.ForeignKey('{{ project_name }}.resources.CartResource', 'cart')
    class Meta:
        queryset = ExtraPriceCart.objects.all()
        resource_name = 'extrapricecart'
        authorization = authorization.StaffAuthorization(all=True)


class ExtraPriceCartItemResource(ModelResource):
    cart_item = fields.ForeignKey('{{ project_name }}.resources.CartItemResource', 'cart_item')
    class Meta:
        queryset = ExtraPriceCartItem.objects.all()
        resource_name = 'extrapricecartitem'
        authorization = authorization.StaffAuthorization(all=True)


class BankAccountResource(ModelResource):
    class Meta:
        resource_name = 'bankaccount'
        queryset = BankAccount.objects.all()
        authorization = authorization.StaffAuthorization(all=True)
        validators = {
            'code': [
                validator.RequiredValidator(message='Kode harus diisi'),
                validator.ModelUniqueValidator(field='code', message='Kode tidak boleh sama'),
            ],
            'name': [
                validator.RequiredValidator(message='Nama Bank harus diisi')
            ],
            'owner': [
                validator.RequiredValidator(message='Nama pemilik rekening harus diisi')
            ],
            'account_number': [
                validator.RequiredValidator(message='Nomor rekening harus diisi'),
                validator.ModelUniqueValidator(field='account_number', message='Nomor rekening tidak boleh sama')
            ]
        }


class PaymentConfirmationResource(ModelResource):
    destination_bank = fields.ForeignKey('{{ project_name }}.resources.BankAccountResource', 'destination_bank')
    class Meta:
        queryset = PaymentConfirmation.objects.all()
        resource_name = 'paymentconfirmation'
        authorization = authorization.StaffAuthorization(all=True)
        filtering = {
            'is_confirmed': ['exact']
        }


class AddressResource(ModelResource):
    class Meta:
        queryset = Address.objects.all()
        resource_name = 'address'
        authorization = authorization.StaffAuthorization(all=True)


class ShopResource(TinyShopResource):
    class Meta(TinyShopResource.Meta):
        pass

api_list = (
    UserResource(),
    CategoryResource(),
    # ImageResource(),
    ProductResource(),
    CartResource(),
    CartItemResource(),
    ExtraPriceCartResource(),
    ExtraPriceCartItemResource(),
    BankAccountResource(),
    PaymentConfirmationResource(),
    AddressResource(),
    ShopResource()
)