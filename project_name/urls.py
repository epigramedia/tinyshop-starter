from django.conf.urls import patterns, include, url
from django.contrib import admin

from tastypie.api import Api
from tinycms.resources import api_list as tinycms_api_list
from .resources import api_list as {{ project_name }}_api_list

from .views import (
    HomeView,
    ProductDetailView,
    ProductListView,
    CartView,
    OrderDetailView,
    OrderListView,
    CheckoutSelectionView,
    CheckoutReviewView,
    CheckoutFinishView,
    PaymentConfirmationView
)

v1_api = Api(api_name='v1')
v1_api.register_multiple(tinycms_api_list)
v1_api.register_multiple({{ project_name }}_api_list)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', '{{ project_name }}.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^products/(?P<slug>[a-zA-Z0-9\-_]+)', ProductDetailView.as_view(), name='product_detail'),
    url(r'^products', ProductListView.as_view(), name='product_list'),
    
    url(r'^cart', CartView.as_view(), name='cart'),
    url(r'^orders/(?P<pk>\d+)', OrderDetailView.as_view(), name='order_detail'),
    url(r'^orders', OrderListView.as_view(), name='order_list'),

    url(r'^checkout/selection', CheckoutSelectionView.as_view(), name='checkout_selection'),
    url(r'^checkout/review', CheckoutReviewView.as_view(), name='checkout_review'),
    url(r'^checkout/finish', CheckoutFinishView.as_view(), name='checkout_finish'),

    url(r'^payment-confirmation', PaymentConfirmationView.as_view(), name='payment_confirmation'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'', include('tinycms.urls'))
)