from django.views.generic import TemplateView
from django.contrib.auth import get_user_model
from tinycms.views import DetailView, ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from .models import *
from tinyshop.api import shop_api
from tinyshop.models import Cart, CartItem, BankAccount

class HomeView(TemplateView):
    template_name = '{{ project_name }}/index.html'


class ProductDetailView(DetailView):
    template_name = '{{ project_name }}/product_detail.html'
    model = Product


class ProductListView(ListView):
    template_name = '{{ project_name }}/product_list.html'
    model = Product


class CartView(TemplateView):
    template_name = '{{ project_name }}/cart.html'

    def get_context_data(self, **kwargs):
        context = super(CartView, self).get_context_data(**kwargs)
        context['cart'] = shop_api.get_cart_item_list(self.request)

        return context


class OrderDetailView(DetailView):
    model = Cart
    template_name = '{{ project_name }}/order_detail.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(OrderDetailView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        User = get_user_model()
        user = User.objects.get(pk=self.request.user.pk)

        queryset = super(OrderDetailView, self).get_queryset()
        queryset = queryset.filter(user=user, is_order=True)

        return queryset

    def get_context_data(self, **kwargs):
        context = super(OrderDetailView, self).get_context_data(**kwargs)

        return context


class OrderListView(ListView):
    model = Cart
    template_name = '{{ project_name }}/order_list.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(OrderListView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        User = get_user_model()
        user = User.objects.get(pk=self.request.user.pk)

        queryset = super(OrderListView, self).get_queryset()
        queryset = queryset.filter(user=user, is_order=True)

        return queryset

    def get_context_data(self, **kwargs):
        context = super(OrderListView, self).get_context_data(**kwargs)

        return context


class CheckoutSelectionView(TemplateView):
    template_name = '{{ project_name }}/checkout/selection.html'

    def dispatch(self, request, *args, **kwargs):
        return super(CheckoutSelectionView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CheckoutSelectionView, self).get_context_data(**kwargs)
        context['cart'] = shop_api.get_cart_item_list(self.request)

        return context


class CheckoutReviewView(TemplateView):
    template_name = '{{ project_name }}/checkout/review.html'

    def get_context_data(self, **kwargs):
        context = super(CheckoutReviewView, self).get_context_data(**kwargs)
        context['cart'] = shop_api.get_cart_item_list(self.request)

        return context


class CheckoutFinishView(TemplateView):
    template_name = '{{ project_name }}/checkout/finish.html'

    def get_context_data(self, **kwargs):
        context = super(CheckoutFinishView, self).get_context_data(**kwargs)
        context['order'] = shop_api.get_latest_submitted_order(self.request)

        return context


class PaymentConfirmationView(TemplateView):
    template_name = '{{ project_name }}/payment_confirmation.html'

    def dispatch(self, request, *args, **kwargs):
        return super(PaymentConfirmationView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PaymentConfirmationView, self).get_context_data(**kwargs)
        context['unpaid_order_list'] = shop_api.get_unpaid_order_list(self.request)
        context['bank_account_list'] = BankAccount.objects.all()

        return context