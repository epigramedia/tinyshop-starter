module.exports = function(grunt) {
    var config = {
        pkg: grunt.file.readJSON('package.json'),

        sass: {
          dist: {
            options: {
              style: 'expanded'
            },
            files: {
              'css/app.css': 'scss/app.scss',
              'css/reusable.css': 'scss/reusable.scss',
              'css/hack.css': 'scss/hack.scss'
            }
          }
        },

        concat: {
            vendors: {
                src: django_concat_js(
                    "../../../templates/<your-theme-name>/_inc/js.html",
                    "concatjs-vendors",
                    "\{\{\ STATIC_URL \}\}<your-theme-name>\/front\/"
                ),
                dest: 'build/vendors.js'
            },
            uis: {
                src: django_concat_js(
                    "../../../templates/<your-theme-name>/_inc/js.html",
                    "concatjs-uis",
                    "\{\{\ STATIC_URL \}\}<your-theme-name>\/front\/"
                ),
                dest: 'build/uis.js'
            },
            apps: {
                src: django_concat_js(
                    "../../../templates/<your-theme-name>/_inc/js.html",
                    "concatjs-apps",
                    "\{\{\ STATIC_URL \}\}<your-theme-name>\/front\/"
                ),
                dest: 'build/apps.js'
            }
        },

        uglify: {
            js: {
                files: {
                    'js/vendors.min.js': [
                        'build/vendors.js'
                    ],
                    'js/uis.min.js' : [
                        'build/uis.js'
                    ],
                    'js/apps.min.js' : [
                        'build/apps.js'
                    ]
                }
            }
        },

        cssmin: {
            options: {
                root: '../../../' // relative to destination file (dest)
            },
            css: {
                src: django_concat_css(
                    "../../../templates/<your-theme-name>/_inc/css.html",
                    "concatcss-styles",
                    "\{\{\ STATIC_URL \}\}<your-theme-name>\/front\/"
                ),
                dest: 'css/styles.min.css'
            }
        },

        watch: {
            css: {
                files: 'scss/*.scss',
                tasks: ['sass']
            }
        }
    };

    grunt.initConfig(config);

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        // 'sass:dist',
        'concat:vendors',
        'concat:uis',
        'concat:apps',
        'uglify:js',
        'cssmin:css'
    ]);
};

// example_params
// var file_name = "../../../templates/<your-theme-name>/_inc/js.html";
// var code = "concatjs-uis";
// var prefix = "\{\{\ STATIC_URL \}\}<your-theme-name>\/front\/";

function django_concat_js(file_name, code, prefix) {
    var fs = require("fs");

    if (prefix === undefined) {
        prefix = "";
    }

    var content = fs.readFileSync(file_name, "utf-8");
    content = content.split("\n");
    content = content.filter(function(x) {return x !== ""});

    // helpers
    var results = [],
        start = false,
        re = new RegExp("<script.*src=\"" + prefix + "(.*\.js)"),
        execute_re;

    for(var i=0; i<content.length; i++) {
        if (content[i] == '<!-- ' + code + ' -->') {
            start = true;
            continue;
        } else if (start && content[i] == '<!-- end -->') {
            break;
        } else if (i == content.length) {
            break;
        } else if (start) {
            execute_re = re.exec(content[i]);
            if (execute_re && execute_re.length > 1) {
                results.push(execute_re[1]);
            }
        }
    }

    return results;
}

function django_concat_css(file_name, code, prefix) {
    var fs = require("fs");

    if (prefix === undefined) {
        prefix = "";
    }

    var content = fs.readFileSync(file_name, "utf-8");
    content = content.split("\n");
    content = content.filter(function(x) {return x !== ""});

    // helpers
    var results = [],
        start = false,
        re = new RegExp("<link.*href=\"" + prefix + "(.*\.css)"),
        execute_re;

    for(var i=0; i<content.length; i++) {
        if (content[i] == '<!-- ' + code + ' -->') {
            start = true;
            continue;
        } else if (start && content[i] == '<!-- end -->') {
            break;
        } else if (i == content.length) {
            break;
        } else if (start) {
            execute_re = re.exec(content[i]);
            if (execute_re && execute_re.length > 1) {
                results.push(execute_re[1]);
            }
        }
    }

    return results;
}
