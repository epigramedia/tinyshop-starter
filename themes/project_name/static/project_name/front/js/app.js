var app = angular.module('app', ['angularFileUpload', 'ng.tastypie', 'vcRecaptcha']);

app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
}]);

app.controller('ParentController', ParentController);
app.controller('HomeController', HomeController);
app.controller('ProductDetailController', ProductDetailController);
app.controller('ProductListController', ProductListController);
app.controller('CartController', CartController);
app.controller('CheckoutSelectionController', CheckoutSelectionController);
app.controller('CheckoutReviewController', CheckoutReviewController);
app.controller('CheckoutFinishController', CheckoutFinishController);
app.controller('OrderDetailController', OrderDetailController);
app.controller('OrderListController', OrderListController);
app.controller('PaymentConfirmationController', PaymentConfirmationController);

app.run(['$rootScope', function($rootScope) {
    $rootScope.message_list = [];

    $rootScope.setError = function(error_response) {
        delete $rootScope.errors;
        $rootScope.errors = {};
        angular.copy(error_response, $rootScope.errors);
    };

    $rootScope.showMessage = function(message, message_type) {
        // remove previous success messages
        $rootScope.message_list = _.filter($rootScope.message_list, function(x) {
            return x.message_type == 'success';
        });

        $rootScope.message_list.unshift({
            message: message,
            message_type: message_type
        });

        // bring to top
        to_top(); // from kutils
    };

    $rootScope.removeMessage = function(index) {
        $rootScope.message_list.splice(index, 1);
    };

    $rootScope.defaultErrorHandler = function(errors) {
        $rootScope.setError(errors);

        var message;

        switch (errors.status) {
            case 400:
                message = 'Input tidak valid!';
                break;
            case 401:
                message = 'Anda tidak memiliki akses!';
                break;
            case 404:
                message = 'Halaman tidak ada';
                break;
            case 403:
                message = 'Forbidden Error!';
                break;
            case 500:
                message = 'Server Error!';
                break;
            case 0:
                message = 'Tidak ada koneksi Internet';
                break;
            default:
                message = 'Terjadi kesalahan dengan kode: ' + errors.status;
        }

        $rootScope.showMessage(message, 'alert');
    };

    $rootScope.$on('$routeChangeStart', function() {
        $rootScope.route_loading = true;
    });

    $rootScope.$on('$routeChangeSuccess', function() {
        $rootScope.route_loading = false;
    });

    $rootScope.$on('$routeChangeError', function(event, preparedRoot, lastRoot) {
        $rootScope.route_loading = false;
        $rootScope.alert_list.push({
            message: 'Maaf, ada kesalahan dalam memproses halaman ' + preparedRoot.loadedTemplateUrl,
            alert_type: 'alert'
        });
    });
}]);