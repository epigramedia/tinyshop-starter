var CartController = ['$scope', '$http', function($scope, $http) {
    $scope.addQuantity = function(product, sku, quantity) {
        $http.post('/api/v1/shop/cart/add/', {
            product: product,
            sku: sku,
            quantity: quantity
        }).then(
            function(results) {
                window.location.reload();
            },
            function(errors) {
                $scope.defaultErrorHandler(errors);
            }
        );
    };

    $scope.increaseQuantity = function(product, sku) {
        $scope.addQuantity(product, sku, 1);
    };

    $scope.decreaseQuantity = function(product, sku) {
        $scope.addQuantity(product, sku, -1);
    };

    $scope.remove = function(product, sku) {
        $http.post('/api/v1/shop/cart/remove/', {
            product: product,
            sku: sku
        }).then(
            function(results) {
                window.location.reload();
            },
            function(errors) {
                $scope.defaultErrorHandler(errors);
            }
        );
    }

    $scope.init = function() {
        console.log('Entering Cart Controller');
    };

    $scope.init();
}];