var CheckoutReviewController = ['$scope', '$http', function($scope, $http) {
    $scope.submit = function() {
        $http.post('/api/v1/shop/submit/').then(
            function(results) {
                window.location.href = '/checkout/finish';
            },
            function(errors) {

            }
        );
    };

    $scope.init = function() {
        console.log('Entering Checkout Review Controller');
    };

    $scope.init();
}];