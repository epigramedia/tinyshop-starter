var CheckoutSelectionController = ['$scope', '$http', function($scope, $http) {
    $scope.getInitialAddress = function() {
        $http.get('/api/v1/shop/checkout/').then(
            function(results) {
                $scope.object = {};
                angular.copy(results.data, $scope.object);
            },
            function(errors) {
                $scope.defaultErrorHandler(errors);
            }
        );
    };

    $scope.submit = function() {
        var saved_object = {};
        angular.copy($scope.object, saved_object);

        if (!$scope.meta.shipping_is_not_billing) {
            angular.copy(saved_object.billing_address, saved_object.shipping_address);
        }

        $http.post('/api/v1/shop/checkout/', saved_object).then(
            function(results) {
                window.location ='/checkout/review/';
            },
            function(errors) {
                $scope.defaultErrorHandler(errors);
            }
        );
    }

    $scope.init = function() {
        console.log('Entering Checkout Selection Controller');
        $scope.meta = {
            shipping_is_not_billing: false
        };

        $scope.getInitialAddress();
    };

    $scope.init();
}];