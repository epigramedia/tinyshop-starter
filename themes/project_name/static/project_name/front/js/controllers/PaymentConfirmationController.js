var PaymentConfirmationController = ['$scope', '$http', 'vcRecaptchaService', function($scope, $http, vcRecaptchaService) {
    $scope.submit = function() {
        var saved_object = {};
        angular.copy($scope.object, saved_object);
        $http.post('/api/v1/shop/payment-confirmation/send', saved_object).then(
            function(results) {
                alert('Thank you for confirming.');
                window.location.href = '/';
            },
            function(errors) {
                $scope.defaultErrorHandler(errors);
            }
        )
    };

    $scope.setWidgetId = function(widget_id) {
        $scope.meta.widget_id = widget_id;
    };

    $scope.setResponse = function(response) {
        $scope.object.recaptcha_response = response;
    };

    $scope.init = function() {
        console.log('Entering Payment Confirmation Controller');
        $scope.object = {
            cart_reference: null,
            recaptcha_response: null,
            sender: null,
            event_date: null,
            bank: null,
            account_number: null,
            destination_account: null,
            total_sent: null,
            notes: null,
            picture: null
        };

        $scope.meta = {
            widget_id: null,
            key: '6LdEVQUTAAAAACGy7302_O5C08jkKq1s1OePPN4o'
        };
    };

    $scope.init();
}];