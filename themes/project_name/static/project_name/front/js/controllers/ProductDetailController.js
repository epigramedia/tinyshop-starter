var ProductDetailController = ['$scope', '$http', function($scope, $http) {
    $scope.addToCart = function() {
        var saved_object = {};
        angular.copy($scope.object, saved_object);
        $http.post('/api/v1/shop/cart/add/', saved_object).then(
            function(results) {
                angular.copy($scope.init_object, $scope.object);
                alert('Product successfully added to cart!');
            },
            function(errors) {
                $scope.defaultErrorHandler(errors);
            }
        );

    };

    $scope.init = function() {
        console.log('Entering Product Detail Controller');
    };

    $scope.init();

    $scope.$watch('init_object', function() {
        $scope.object = {};
        if (angular.isObject($scope.init_object)) {
            angular.copy($scope.init_object, $scope.object);
        }
            
    }, true);
}];